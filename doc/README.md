Brent documentation
-------------------

The docbook source in `brent.xml` is used to generate the Unix
man-page `brent.3` and the plain text `brent.txt`. The tools 
`xsltproc` and `lynx` are required to regenerate these files.

The script `brent-fetch.sh` pulls the latest version of `brent.c`
and `brent.h` from the GitHub master to the current directory.