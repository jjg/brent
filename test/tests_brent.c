/*
  cunit tests for brent.c
  J.J.Green 2015
*/

#include <math.h>
#include <brent.h>

#include "tests_brent.h"

CU_TestInfo tests_brent[] =
  {
    {"quadratic objective",    test_brent_quadratic},
    {"SciPy objective",        test_brent_scipy},
    {"Gaussian objective",     test_brent_gaussian},
    {"bad bracket"       ,     test_brent_bad_bracket},
    {"failed evaluation",      test_brent_bad_eval},
    {"maximum iterations",     test_brent_maxiter},
    {"non-positive tolerance", test_brent_nptol},
    CU_TEST_INFO_NULL,
  };

static double tols[] = { 1e-1, 1e-2, 1e-3, 1e-4, 1e-6, 1e-8 };
static size_t ntols = sizeof(tols)/sizeof(double);

/* some example objectives */

static int quadratic(double x, void *arg, double *fx)
{
  *fx = x*x;
  return 0;
}

static int gaussian(double x, void *arg, double *fx)
{
  *fx = 1 - exp(-x*x);
  return 0;
}

static void check_function(objective_t *f, double B[3], double tol, double x)
{
  double x0;
  const brent_opt_t opt = {
    .tolerance = tol,
    .max_iterations = 500
  };
  brent_stat_t stat = {0};
  int res = brent(f, NULL, B, &opt, &x0, &stat);

  CU_ASSERT_EQUAL(res, BRENT_OK);
  CU_ASSERT_DOUBLE_EQUAL(x, x0, tol);
  CU_ASSERT(stat.iterations > 0);
  CU_ASSERT(stat.evaluations > 0);
}

extern void test_brent_quadratic(void)
{
  double B[3] = {-1, 0, 1};
  for (size_t i = 0 ; i < ntols ; i++)
    check_function(quadratic, B, tols[i], 0.0);
}

extern void test_brent_gaussian(void)
{
  double B[3] = {-1, 0, 1};
  for (size_t i = 0 ; i < ntols ; i++)
    check_function(gaussian, B, tols[i], 0.0);
}

/* a test from the scipy test-suite */

static int scipy(double x, void *arg, double *fx)
{
  double z = x - 1.5;
  *fx = z*z - 0.8;
  return 0;
}

extern void test_brent_scipy(void)
{
  double B[3] = {1, 1.5, 2};
  check_function(scipy, B, 1e-6, 1.5);
}

/* correct behaviour when passed a bad bracket */

static void check_bad_bracket(const double B[3])
{
  double x0 = -1;
  brent_stat_t stat = {0};
  int res = brent(quadratic, NULL, B, NULL, &x0, &stat);

  CU_ASSERT_EQUAL(res, BRENT_FAIL_BRACKET);
  CU_ASSERT_EQUAL(x0, -1);
  CU_ASSERT_EQUAL(stat.iterations, 0);
  CU_ASSERT_EQUAL(stat.evaluations, 0);
}

extern void test_brent_bad_bracket(void)
{
  const double B[][3] =
    {
      {1, 1, 1},
      {1, 1, 2},
      {1, 2, 2},
      {3, 2, 1},
      {2, 3, 1},
      {1, 3, 2}
    };

  size_t n = sizeof(B)/sizeof(double[3]);
  for (size_t i = 0 ; i < n ; i++)
    check_bad_bracket(B[i]);
}

/* correct behaviour when the objective returns non-zero */

static int return_nonzero(double x, void *arg, double *fx)
{
  *fx = 3.0;
  return 1;
}

extern void test_brent_bad_eval(void)
{
  brent_stat_t stat = {0};
  const double B[3] = {0, 1, 2};
  double x0 = 7;
  int res = brent(return_nonzero, NULL, B, NULL, &x0, &stat);

  CU_ASSERT_EQUAL(res, BRENT_FAIL_EVAL);
  CU_ASSERT_EQUAL(x0, 7);
  CU_ASSERT_EQUAL(stat.iterations, 0);
  CU_ASSERT_EQUAL(stat.evaluations, 0);
}

/* termination when max_iterations exceeeded */

extern void test_brent_maxiter(void)
{
  brent_stat_t stat = {0};
  const brent_opt_t opt = {
    .max_iterations = 3,
    .tolerance = 1e-8
  };
  const double B[3] = {-1, 0, 1};
  double x0 = 7;
  int res = brent(gaussian, NULL, B, &opt, &x0, &stat);

  CU_ASSERT_EQUAL(res, BRENT_FAIL_MAXITER);
  CU_ASSERT_EQUAL(x0, 7);
  CU_ASSERT_EQUAL(stat.iterations, 0);
  CU_ASSERT_EQUAL(stat.evaluations, 0);
}

/* when the tolerance option is not positive*/

extern void test_brent_nptol(void)
{
  brent_stat_t stat = {0};
  const brent_opt_t opt = {
    .max_iterations = 3,
    .tolerance = 0.0
  };
  const double B[3] = {-1, 0, 1};
  double x0 = 7;
  int res = brent(gaussian, NULL, B, &opt, &x0, &stat);

  CU_ASSERT_EQUAL(res, BRENT_FAIL_NPTOL);
  CU_ASSERT_EQUAL(x0, 7);
  CU_ASSERT_EQUAL(stat.iterations, 0);
  CU_ASSERT_EQUAL(stat.evaluations, 0);
}
